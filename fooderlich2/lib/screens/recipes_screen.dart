import 'package:flutter/material.dart';
import 'package:fooderlich2/api/mock_fooderlich_service.dart';
import 'package:fooderlich2/components/recipes_grid_view.dart';
import 'package:fooderlich2/models/simple_recipe.dart';

class RecipesScreen extends StatelessWidget {
  final exploreService = MockFooderlichService();

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: exploreService.getRecipes(),
        builder: (context, AsyncSnapshot<List<SimpleRecipe>> snapshot) {
          if (snapshot.connectionState == ConnectionState.done) {
            return RecipesGridView(recipes: snapshot.data ?? []);
          } else {
            return Center(
              child: CircularProgressIndicator(),
            );
          }
        });
  }
}
